import styled, {css} from "styled-components";

export const HeaderSC = styled.header<{bgSrc: string}>`
    display: flex;
    padding: 2rem;
    background-image: ${({bgSrc}) => bgSrc && css`url(${bgSrc})`};
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
    height: 51.9rem;
`;



export const NavSC = styled.nav`
    flex: 1;
    padding: 0 1rem;
    
`;

export const UlSC = styled.ul`
    padding: 0;
    margin: 0;
    gap: 3rem;
    font-size: 3rem;
    justify-content: center;
    list-style: none;
    display: flex;
    /* font-family: cursive; */
    font-weight: bold;
    a{
        text-decoration: none;
        &.active{
            color: #1cabcd;
        }
    }
`;