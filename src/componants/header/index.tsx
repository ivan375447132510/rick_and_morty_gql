import { HeaderSC, NavSC, UlSC } from "./styled";
import head from "../../assets/images/head.jpg";
import { NavLink } from "react-router-dom";

// IntrinsicAttributes

interface ICreateMenu {
  title: string;
  link: string;
  exact?: boolean;
}

const createMenu = [
  {
    title: "главная",
    link: "/",
    exact: true,
  },
  {
    title: "персонажи",
    link: "/characters",
  },
  {
    title: "локации",
    link: "/locations",
  },
  {
    title: "эпизоды",
    link: "/episodes",
  },
] as Array<ICreateMenu>;

export const Header = () => {
  return (
    <HeaderSC bgSrc={head}>
      <NavSC>
        <UlSC>
          {createMenu.map((item: ICreateMenu, idx: number) => (
            <li key={idx}>
              <NavLink
                activeClassName={"active"}
                exact={item.exact && item.exact}
                to={item.link}
              >
                {item.title}
              </NavLink>
            </li>
          ))}
        </UlSC>
      </NavSC>
    </HeaderSC>
  );
};
