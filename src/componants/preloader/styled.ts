import styled from "styled-components";

export const PreloaderSC = styled.div`
    color: #1cabcd;
    font-size: 5rem;
    text-transform: uppercase;
    padding: 10rem;
`;