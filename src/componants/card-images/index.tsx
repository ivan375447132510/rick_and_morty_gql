import { CardImagesSC } from "./styled";

interface ICardImages {
  image: string;
  alt?: string;
}

export const CardImages = (props: ICardImages) => {
  const { image, alt } = props;

  return (
    <CardImagesSC>
      <img width="100%" src={image} alt={alt || "img"} />
    </CardImagesSC>
  );
};
