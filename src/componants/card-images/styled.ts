import styled from "styled-components";

export const CardImagesSC = styled.div`
    display: flex;
    text-align: center;
    justify-content: center;
    img{ 
        max-width: 100%;
        max-height: 100%;
        width: 30rem;
        height: 30rem;
    }
`;