import { NavLink } from "react-router-dom";
import { TypenameCategories } from "../../init/type-script/types";
import { CardImages } from "../card-images";
import { CardSC, CharacterInfoSC } from "./styled";

// TODO: Remove any
interface ICategoriesCard {
  categorie: any;
  baseUrl: string;
}

const useTypeName = (type: TypenameCategories) => {
  switch (type) {
    case TypenameCategories.Character:
      return "Name";
    case TypenameCategories.Location:
      return "Location";
    case TypenameCategories.Episode:
      return "Episodes";
    default:
      return "";
  }
};

export const CategoriesCard = (props: ICategoriesCard) => {
  const { categorie, baseUrl } = props;
  const typeName = useTypeName(categorie.__typename);
  const categoriesCardJSX = (
    <>
      {Object.keys(categorie).map((element: string, idx: number) => {
        switch (element) {
          case "__typename":
            return null;
          case "image":
            return null;
          case "id":
            return null;
          case "name":
            return null;
          default:
            return (
              <CharacterInfoSC key={idx}>
                {element}: {categorie[element]}
              </CharacterInfoSC>
            );
        }
      })}
    </>
  );

  return (
    <CardSC>
      <NavLink to={`/${baseUrl}/${categorie.id}`}>
        {categorie.image && (
          <CardImages image={categorie.image} alt={categorie.name} />
        )}
        <CharacterInfoSC>
          {typeName}: {categorie.name}
        </CharacterInfoSC>
        {categoriesCardJSX}
      </NavLink>
    </CardSC>
  );
};
