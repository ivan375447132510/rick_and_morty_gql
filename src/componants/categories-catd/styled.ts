import styled from "styled-components";

export const CardSC = styled.div`
    font-size: 3rem;
    margin: 3rem 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 1rem;
    width: 50rem;
    * {
        text-decoration: none;
        color: #1cabcd;
    }
`;

export const CharacterInfoSC = styled.div`
    text-align: center;
    margin-top: 1rem;
`;