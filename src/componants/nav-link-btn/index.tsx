import { NavLinkBtnSC } from "./styled";

interface IProps {
  linkPath: string;
  children: string | React.ReactNode;
  className?: string;
}

export const NavLinkBtn = (props: IProps) => {
  const { linkPath, children, className } = props;

  return (
    <NavLinkBtnSC className={className} to={linkPath}>
      {children}
    </NavLinkBtnSC>
  );
};
