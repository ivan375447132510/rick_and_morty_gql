import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { NavLinkBtn } from ".";

export const NavLinkBtnSC = styled(NavLink)`
    color: #fff;
    font-size: 3rem;
    text-decoration: none;
    text-transform: uppercase;
    background: linear-gradient(to bottom,#64d0da 0%,#01717b 100%);
    padding: 1rem 2rem;
    border-radius: 2.4rem;
    display: flex;
    width: max-content;
`;

export const NavLinkBtnCenter = styled(NavLinkBtn)`
    margin: 1rem auto 3rem;
`;