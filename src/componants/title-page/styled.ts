import styled from "styled-components";

export const TitleSC = styled.div`
    background: linear-gradient(to bottom, #64d0da 0%, #01717b 100%);
    padding: 1rem;
    font-size: 2.5rem;
    line-height: 1.5;
    cursor: default;
    color: #fff;
`;