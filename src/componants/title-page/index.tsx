import { TitleSC } from "./styled";

interface IProps {
  title: string;
}

export const TitlePage = (props: IProps) => {
  const { title } = props;

  return <TitleSC>{title}</TitleSC>;
};
