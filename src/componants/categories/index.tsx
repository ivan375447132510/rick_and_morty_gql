import { TitlePage } from "../title-page";
import { ContentSC } from "./styled";
import { ICategoriesResults } from "../../init/type-script/types";
import { CategoriesCard } from "../categories-catd";

interface IProps {
  title: string;
  categories: ICategoriesResults[];
  baseUrl: string;
}

export const Categories = (props: IProps) => {
  const { title, categories, baseUrl } = props;
  return (
    <>
      <TitlePage title={title} />
      <ContentSC>
        {categories?.map((categorie: ICategoriesResults) => (
          <CategoriesCard
            key={categorie.id}
            categorie={categorie}
            baseUrl={baseUrl}
          />
        ))}
      </ContentSC>
    </>
  );
};
