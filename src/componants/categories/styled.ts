import styled from "styled-components";

export const ContentSC = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 0 1.5rem;
`;