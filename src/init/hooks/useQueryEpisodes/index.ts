import { useLazyQuery } from "@apollo/react-hooks";
import { loader } from "graphql.macro";
import { IEpisodes, IGetLazyQueryOptions } from "../../type-script/types";

const queryEpisodes = loader("./gql/useQueryEpisodes.gql");

// TODO remove any
interface _IEpisodes {
  loading: boolean;
  episodes: IEpisodes;
  getEpisodes: (options: IGetLazyQueryOptions) => void;
  error: any;
}

export const useQueryEpisodes: () => _IEpisodes = () => {
  const [getEpisodes, { loading, data, error }] = useLazyQuery(queryEpisodes);

  return {getEpisodes, loading, episodes: data?.episodes, error };
};
