import { IGetLazyQueryOptions, ILocations } from './../../type-script/types';
import {useLazyQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const queryLocations = loader("./gql/useQueryLocations.gql");


// TODO remove any
interface _ILocations {
    loading: boolean, 
    locations: ILocations, 
    getLocations: (options: IGetLazyQueryOptions) => void, 
    error: any
}

export const useQueryLocations: () => _ILocations  = () => {
    const [getLocations, {loading, data, error}] = useLazyQuery(queryLocations);

    return {getLocations, loading, locations: data?.locations, error}
}