import {useLazyQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";
import {ICharacters, IGetLazyQueryOptions} from "../../type-script/types";

const queryCharacters = loader("./gql/useQueryCharacters.gql");

// TODO remove any
interface _ICharacters {
    loading: boolean, 
    characters: ICharacters, 
    getCharacters: (options: IGetLazyQueryOptions) => void;
    error: any
}

export const useQueryCharacters: () => _ICharacters  = () => {
    const [getCharacters ,{loading, data, error}] = useLazyQuery(queryCharacters);

    return {getCharacters, loading, characters: data?.characters, error}
}