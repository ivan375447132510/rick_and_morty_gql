import styled, {css} from "styled-components";

export const ContentSC = styled.div<{bgSrc: string}>`
    height: 100%;
    width: 100%;
    flex: 1;
    background-image: ${({bgSrc}) => bgSrc && css`url(${bgSrc})`};
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
`;