import { Switch, Route } from "react-router-dom";
import { ContentSC } from "./styled";
import bgimg from "../../assets/images/bg.jpg";
import { Home, Characters } from "../../pages";

export const Routers = () => {
  return (
    <ContentSC bgSrc={bgimg}>
      <Switch>
        <Route path="/" exact render={() => <Home />} />
        <Route path="/characters" exact render={() => <Characters />} />
        <Route path="/locations" render={() => <> локации </>} />
        <Route path="/episodes" render={() => <> эпизоды </>} />
      </Switch>
    </ContentSC>
  );
};
