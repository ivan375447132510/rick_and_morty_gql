export interface IInfo {
    count: number | null;
    pages: number | null;
    next: number | null;
    prev: number | null;
}

// ICharacters
export interface ICharactersResults {
    id: number | string;
    name: string;
    status: string;
    image: string;
    __typename: TypenameCategories;
}

export interface ICharacters {
    info: IInfo;
    results: Array<ICharactersResults>;
}
// end

// ILocations
export interface ILocationsResults {
    id: number | string;
    name: string;
    type: string;
    dimension: string;
    __typename: TypenameCategories;
}

export interface ILocations {
    info: IInfo;
    results: Array<ILocationsResults>;
}
// end

// IEpisodes
export interface IEpisodesResults {
    id: number | string;
    name: string;
    air_date: string;
    episode: string;
    __typename: TypenameCategories;
}

export interface IEpisodes {
    info: IInfo;
    results: Array<IEpisodesResults>;
}

// end

export type ICategoriesResults = ICharactersResults | ILocationsResults | IEpisodesResults;

//  enum name categories
export enum TypenameCategories {
    Character = "Character",
    Location = "Location",
    Episode = "Episode"
}

export interface IGetLazyQueryOptions {
    variables: {
        page: number;
    }
}