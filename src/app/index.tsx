import { MainBox } from "./styled";
import { Routers } from "../init/routers";
import { Header } from "../componants";

export const App = () => {
  return (
    <MainBox>
      <Header />
      <Routers />
    </MainBox>
  );
};
