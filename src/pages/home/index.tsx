import { Characters, Locations, Episodes } from "./components";

export const Home = () => {
  return (
    <>
      <Characters />
      <Locations />
      <Episodes />
    </>
  );
};
