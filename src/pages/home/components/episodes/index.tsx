import { useEffect } from "react";
import {
  Categories,
  NavLinkBtnCenter,
  Preloader,
} from "../../../../componants";
import { useQueryEpisodes } from "../../../../init/hooks/useQueryEpisodes";

export const Episodes = () => {
  const { getEpisodes, loading, episodes } = useQueryEpisodes();

  useEffect(() => {
    getEpisodes({
      variables: {
        page: 1,
      },
    });
  }, [getEpisodes]);

  return (
    <>
      {loading && <Preloader />}
      <Categories
        title={"эпизоды"}
        baseUrl="episodes"
        categories={episodes?.results}
      />
      {!loading && (
        <NavLinkBtnCenter linkPath="/episodes">все эпизоды</NavLinkBtnCenter>
      )}
    </>
  );
};
