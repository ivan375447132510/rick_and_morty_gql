import { useEffect } from "react";
import {
  Categories,
  Preloader,
  NavLinkBtnCenter,
} from "../../../../componants";
import { useQueryCharacters } from "../../../../init/hooks/useQueryCharacters";

export const Characters = () => {
  const { getCharacters, loading, characters } = useQueryCharacters();

  useEffect(() => {
    getCharacters({
      variables: {
        page: 1,
      },
    });
  }, [getCharacters]);

  return (
    <>
      {loading && <Preloader />}
      <Categories
        title={"персонажи"}
        baseUrl="characters"
        categories={characters?.results}
      />
      {!loading && (
        <NavLinkBtnCenter linkPath="/characters">
          все персонажи
        </NavLinkBtnCenter>
      )}
    </>
  );
};
