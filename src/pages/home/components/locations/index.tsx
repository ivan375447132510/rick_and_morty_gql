import { useEffect } from "react";
import {
  Categories,
  Preloader,
  NavLinkBtnCenter,
} from "../../../../componants";
import { useQueryLocations } from "../../../../init/hooks/useQueryLocations";

export const Locations = () => {
  const { getLocations, loading, locations } = useQueryLocations();

  useEffect(() => {
    getLocations({
      variables: {
        page: 1,
      },
    });
  }, [getLocations]);

  return (
    <>
      {loading && <Preloader />}
      <Categories
        title={"локации"}
        baseUrl="locations"
        categories={locations?.results}
      />
      {!loading && (
        <NavLinkBtnCenter linkPath="/locations">все локации</NavLinkBtnCenter>
      )}
    </>
  );
};
