import { useEffect, useState } from "react";
import { Categories, Preloader } from "../../componants";
import { useQueryCharacters } from "../../init/hooks/useQueryCharacters";

export const Characters = () => {
  const [curentPage, setCurentPage] = useState<number>(1);
  const { getCharacters, loading, characters } = useQueryCharacters();

  const pages = characters?.info.pages;
  const arrayPages = new Array(pages).fill(1);

  useEffect(() => {
    getCharacters({
      variables: {
        page: curentPage,
      },
    });
  }, [curentPage, getCharacters]);

  const nextPage = (page: number) => {
    if (page !== curentPage) {
      setCurentPage(page);
    }
  };

  if (loading) {
    return <Preloader />;
  }

  return (
    <>
      <Categories
        title={"персонажи"}
        baseUrl="characters"
        categories={characters?.results}
      />
      <div
        style={{
          display: "flex",
          color: "#fff",
          fontSize: "2rem",
          width: "70rem",
          margin: "1rem auto 2rem",
          overflow: "auto",
        }}
      >
        {arrayPages?.map((_, index: number) => {
          const newIndex = index + 1;
          return (
            <div
              style={{
                padding: "1rem",
                margin: "rem",
                color: newIndex === curentPage ? "red" : "green",
              }}
              key={newIndex}
              onClick={() => nextPage(newIndex)}
            >
              {newIndex}
            </div>
          );
        })}
      </div>
    </>
  );
};
